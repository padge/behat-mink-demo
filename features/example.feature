Feature: Example feature
  Some details about the feature

  Scenario: Navigating to example page
    Given I am on "index.html"
    When I follow "Example Page"
    Then I should be on "example.html"
    And I should see "Example Page"
    And I should see "Some text"
    And I should not see "Foo"
    And I should see "Back"
