# Behat + Mink Example

Ensure Apache is serving this url: **http://localhost/behat-mink-demo/index.html**

Download composer

    $ curl http://getcomposer.org/installer | php

Install dependencies

    $ php composer.phar install

Run PhantomJS

    $ phantomjs --webdriver=8643 > /dev/null &

Run Behat

    $ bin/behat
